<div class="c-members-box__icon-social">
    <ul class="c-members-box__icon-social-list">
        <li class="c-members-box__icon-social-list__item">
            <figure class="c-members-box__icon-social-list__item__icon">
                <svg class="o-icon o-icon--dribble">
                    <use xlink:href="./static/symbol/svg/sprite.symbol.svg#icon-dribble">
                    </use>
                </svg>
            </figure>
        </li>
        <li class="c-members-box__icon-social-list__item">
            <figure class="c-members-box__icon-social-list__item__icon">
                <svg class="o-icon o-icon--twitter">
                    <use xlink:href="./static/symbol/svg/sprite.symbol.svg#icon-twitter">
                    </use>
                </svg>
            </figure>
        </li>
        <li class="c-members-box__icon-social-list__item">
            <figure class="c-members-box__icon-social-list__item__icon">
                <svg class="o-icon o-icon--contact">
                    <use xlink:href="./static/symbol/svg/sprite.symbol.svg#icon-contact">
                    </use>
                </svg>
            </figure>
        </li>
    </ul>
</div>
