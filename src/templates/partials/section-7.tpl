<div class="c-advance-wrapper">
    <ul class="c-list c-list--advance">
        <li class="c-list__item">
            <div class="c-list__item__number">
                <h6 class="t-heading-11">12,458+</h6>
            </div>
            <div class="c-list__item__text">
                <p class="t-text-7">Projects Completed</p>
            </div>
        </li>
        <li class="c-list__item">
            <div class="c-list__item__number">
                <h6 class="t-heading-11">1,796+</h6>
            </div>
            <div class="c-list__item__text">
                <p class="t-text-7">Satisfied Clients</p>
            </div>
        </li>
        <li class="c-list__item">
            <div class="c-list__item__number">
                <h6 class="t-heading-11">1,000+</h6>
            </div>
            <div class="c-list__item__text">
                <p class="t-text-7">Positive Feedbacks</p>
            </div>
        </li>
        <li class="c-list__item">
            <div class="c-list__item__number">
                <h6 class="t-heading-11">1,500+</h6>
            </div>
            <div class="c-list__item__text">
                <p class="t-text-7">Freebies Released</p>
            </div>
        </li>
    </ul>
</div>