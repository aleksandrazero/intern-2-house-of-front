                <div class="c-list-wrapper">
                    <ul class="c-list c-list--icon">
                        <li class="c-list__item">
                            <figure class="c-list__item__img">
                                <img src="./static/img/icon-price.png" alt="Icon-price" title="Icon price">
                            </figure>
                            <div class="c-list__item__desc">
                                <p class="t-text-3 t-upper">fixed price projects</p>
                            </div>
                        </li>
                        <li class="c-list__item">
                            <figure class="c-list__item__img">
                                <img src="./static/img/icon-time.png" alt="Icon-time" title="Icon time">
                            </figure>
                            <div class="c-list__item__desc">
                                <p class="t-text-3 t-upper">recieve on time</p>
                            </div>
                        </li>
                        <li class="c-list__item">
                            <figure class="c-list__item__img">
                                <img src="./static/img/icon-satisfaction.png" alt="Icon-satisfaction" title="Icon satisfaction">
                            </figure>
                            <div class="c-list__item__desc">
                                <p class="t-text-3 t-upper">satisfaction guranted</p>
                            </div>
                        </li>
                    </ul>
                </div>
