<div class="c-contact-wrapper">
    <div class="c-contact-title">
        <h3 class="t-heading-7">Keep in Touch<h3>
    </div>
    <div class="c-contact-list">
        <ul class="c-list c-list--contact">
            <li class="c-list__item">
                <form class="f-form f-form--name">
                    <input class="f-form__input t-text-8" type="text" name="name" value="Name">
                </form>
                <form class="f-form f-form--e-mail">
                    <input class="f-form__input f-form__input--e-mail t-text-8" type="text" name="e-mail" value="E-mail">
                </form>
            </li>
            <li class="c-list__item c-list__item--message">
                <form class="f-form f-form--message">
                    <input class="f-form__input t-text-8" type="text" name="message" value="Message">
                </form>
            </li>
        </ul>
    </div>
    <div class="c-contact-button">
        <button class="c-button"><span>Send Message</span></button>
    </div>
</div>
