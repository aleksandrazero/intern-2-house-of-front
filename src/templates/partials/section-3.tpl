<div class="c-services-wrapper">
    <div class="c-services-desc">
        <div class="c-services-desc__title">
            <h3 class="t-heading-7">Services</h3>
        </div>
        <div class="c-services-desc__text">
            <p class="t-text-4">We understand your requirment and provide quality works.<p>
        </div>
    </div>
    <div class="c-services-list">
        <ul class="c-list c-list--services">
            <li class="c-list__item">
                <div class="c-services-box">
                    <div class="c-services-box__content">
                        <figure class="c-services-box__icon">
                            <svg class="o-icon o-icon--webdesign">
                                <use xlink:href="./static/symbol/svg/sprite.symbol.svg#icon-webdesign">
                                </use>
                            </svg>
                        </figure>
                        <div class="c-services-box__title">
                            <h4 class="t-heading-6">Web Design</h4>
                        </div>
                        <div class="c-services-box__text">
                            <p>Analytics release series A financing launch party interaction design android angel investor.</p>
                        </div>
                    </div>
                </div>
                <div class="c-services-border-bottom"></div>
            </li>
            <li class="c-list__item">
                <div class="c-services-box">
                    <div class="c-services-box__content">
                        <figure class="c-services-box__icon">
                            <svg class="o-icon o-icon--uxdesign">
                                <use xlink:href="./static/symbol/svg/sprite.symbol.svg#icon-uxdesign">
                                </use>
                            </svg>
                        </figure>
                        <div class="c-services-box__title">
                            <h4 class="t-heading-6">UX Design</h4>
                        </div>
                        <div class="c-services-box__text">
                            <p>Analytics release series A financing launch party interaction design android angel investor.</p>
                        </div>
                    </div>
                </div>
                <div class="c-services-border-bottom"></div>
            </li>
            <li class="c-list__item">
                <div class="c-services-box">
                    <div class="c-services-box__content">
                        <figure class="c-services-box__icon">
                            <svg class="o-icon o-icon--photo">
                                <use xlink:href="./static/symbol/svg/sprite.symbol.svg#icon-photo">
                                </use>
                            </svg>
                        </figure>
                        <div class="c-services-box__title">
                            <h4 class="t-heading-6">Photogrpahy</h4>
                        </div>
                        <div class="c-services-box__text">
                            <p>Analytics release series A financing launch party interaction design android angel investor.</p>
                        </div>
                    </div>
                </div>
                <div class="c-services-border-bottom"></div>
            </li>
            <li class="c-list__item">
                <div class="c-services-box">
                    <div class="c-services-box__content">
                        <figure class="c-services-box__icon">
                            <svg class="o-icon o-icon--appdevelopment">
                                <use xlink:href="./static/symbol/svg/sprite.symbol.svg#icon-appdevelopment">
                                </use>
                            </svg>
                        </figure>
                        <div class="c-services-box__title">
                            <h4 class="t-heading-6">App Development</h4>
                        </div>
                        <div class="c-services-box__text">
                            <p>Analytics release series A financing launch party interaction design android angel investor.</p>
                        </div>
                    </div>
                </div>
                <div class="c-services-border-bottom"></div>
            </li>
        </ul>
    </div>
</div>
