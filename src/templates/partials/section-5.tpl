<div class="c-solutions-wrapper">
    <div class="c-solutions-desc">
        <div class="c-solutions-desc__title">
            <h3 class="t-heading-9">Technical Solutions Exclusively for Agencies</h3>
        </div>
        <div class="c-solutions-desc__text-1">
            <p class="t-text-2">Focus on core of your bussines: getting new clients, building portfolio, polishing the designs - let us care about technology.</p>
        </div>
        <div class="c-solutions-desc__text-2">
            <p class="t-text-2">Not every project should be done by your core team. You dont't need to resign from projects because of lack of work force anymore.</p>
        </div>
        <div class="c-solutions-desc__button">
            <button class="c-button"><span>Learn More</span></button>
        </div>
    </div>
</div>