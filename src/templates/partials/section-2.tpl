<div class="c-placework-wrapper">
    <div class="c-placework-desc">
        <div class="c-placework-desc__title">
            <h3 class="t-heading-4">Developing in the heart od Europe</h3>
        </div>
        <div class="c-placework-desc__text">
            <p class="t-text-2">Having worked for clients world wide, from New York to Singapure, we're seeking new challeng- es. Because it's time to redefine IT outsourcing and make it ft into one sentence right next to the word "enjoyable".</p>
        </div>
        <div class="c-placework-desc__button">
            <button class="c-button"><span>Learn More</span></button>
        </div>
    </div>
</div>
