<div class="c-focus-wrapper">
    <div class="c-focus">
        <div class="c-focus__title">
            <h3 class="t-heading-7">Our Focus</h3>
        </div>
        <div class="c-focus__list">
            <ul class="c-list c-list--focus">
                <li class="c-list__item c-list__item--reverse">
                    <div class="c-list__item__rectangle">
                        <div class="c-list__rectangle__circle"></div>
                    </div>
                    <figure class="c-list__item__icon">
                        <img src="./static/img/icon-light.png" alt="icon-light" title="Icon Light">
                    </figure>
                    <div class="c-list__item__title">
                        <h5 class="t-heading-8">we're creative</h5>
                    </div>
                    <div class="c-list__item__text">
                        <p class="t-text-5">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium</p>
                    </div>
                </li>
                <li class="c-list__item">
                    <div class="c-list__item__rectangle">
                        <div class="c-list__rectangle__circle"></div>
                    </div>
                    <figure class="c-list__item__icon">
                        <img src="./static/img/icon-light.png" alt="icon-light" title="Icon Light">
                    </figure>
                    <div class="c-list__item__title">
                        <h5 class="t-heading-8">we're creative</h5>
                    </div>
                    <div class="c-list__item__text">
                        <p class="t-text-5">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium</p>
                    </div>
                </li>
                <li class="c-list__item c-list__item--reverse">
                    <div class="c-list__item__rectangle">
                        <div class="c-list__rectangle__circle"></div>
                    </div>
                    <figure class="c-list__item__icon">
                        <img src="./static/img/icon-light.png" alt="icon-light" title="Icon Light">
                    </figure>
                    <div class="c-list__item__title">
                        <h5 class="t-heading-8">we're creative</h5>
                    </div>
                    <div class="c-list__item__text">
                        <p class="t-text-5">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium</p>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
