<div class="c-members-wrapper">
    <div class="c-members-desc">
        <div class="c-members-desc__title">
            <h3 class="t-heading-7">Our Team Member</h3>
        </div>
        <div class="c-members-desc__text">
            <p class="t-text-6">We understand your requirement and provide quality works.</p>
        </div>
    </div>
    <div class="c-members-container">
        <ul class="c-list c-list--members">
            <li class="c-list__item c-list__item--members">
                <div class="c-members-box">
                    <div class="c-members-box__portrait">
                        <img src="./static/img/member-1.png" alt="member" title="Our Team Member">
                    </div>
                    <div class="c-members-box__title">
                        <h5 class="t-heading-10">DAVID SMITH</h5>
                    </div>
                    <div class="c-members-box__text">
                        <p class="t-text-5">Analystics release series A financing launch party interaction design android angel investor.</p>
                    </div>
                    {% include "partials/icon-social-list.tpl" %}
                </div>
            </li>
            <li class="c-list__item c-list__item--members">
                <div class="c-members-box">
                    <div class="c-members-box__portrait">
                        <img src="./static/img/member-1.png" alt="member" title="Our Team Member">
                    </div>
                    <div class="c-members-box__title">
                        <h5 class="t-heading-10">POPEY KHAJI</h5>
                    </div>
                    <div class="c-members-box__text">
                        <p class="t-text-5">Analystics release series A financing launch party interaction design android angel investor.</p>
                    </div>
                    {% include "partials/icon-social-list.tpl" %}
                </div>
            </li>
            <li class="c-list__item c-list__item--members">
                <div class="c-members-box">
                    <div class="c-members-box__portrait">
                        <img src="./static/img/member-1.png" alt="member" title="Our Team Member">
                    </div>
                    <div class="c-members-box__title">
                        <h5 class="t-heading-10">RAHABI KHAN</h5>
                    </div>
                    <div class="c-members-box__text">
                        <p class="t-text-5">Analystics release series A financing launch party interaction design android angel investor.</p>
                    </div>
                    {% include "partials/icon-social-list.tpl" %}
                </div>
            </li>
        </ul>
    </div>
</div>
