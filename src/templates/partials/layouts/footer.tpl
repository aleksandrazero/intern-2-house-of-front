{% macro render(_active_no) %}
    {% if _active_no == '' %}
        {% set logo_href='#' %}
    {% else %}
        {% set logo_href='index.html' %}
    {% endif %}
		<footer class="l-footer">
            <div class="l-inner u-gutter-md-top u-gutter-xs-bottom">
                <div class="c-footer">
                    <p class="t-text-1">&copy; 2017 All rights reserved.</p>
                </div>
            </div>
        </footer>
{% endmacro %}