{% macro render(_active_no) %}
    {% if _active_no == '' %}
        {% set logo_href='#' %}
    {% else %}
        {% set logo_href='index.html' %}
    {% endif %}
		<header class="l-header">
            <div class="l-inner u-gutter-s-top u-gutter-xl-bottom">
                <div class="c-header">
                    <div class="c-header__logo">
                        <img src="./src/img/logo.png" alt="logo PagePro" title="Logo PagePro">
                    </div>
                    <div class="c-header__title">
                        <h3 class="t-heading-1">We are helping agencies</h3>
                        <h1 class="t-heading-2">House of front-end development</h1>
                        <h4 class="t-heading-3">Agencies – design, digital, advertising, we are your front-end development partner.</h4>
                    </div>
                    <div class="c-header__button">
                        <button class="c-button"><span>Learn More</span></button>
                    </div>
                </div>
            </div>
        </header>
{% endmacro %}
