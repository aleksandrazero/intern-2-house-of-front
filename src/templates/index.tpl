<!doctype html>
<html lang="en" class="no-js">
    <head>
        <meta charset="utf-8">
        <title>{% block title %} {{ title }} {% endblock %}</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
        {% include "head/head-links.tpl" %}
        <script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>
    </head>
    <body>
        {% import "partials/layouts/header.tpl" as l_header %}
        {{ l_header.render() }}
        <section class="l-section l-section--s-1">
            <div class="l-inner ">
                {% include "partials/icon-box.tpl" %}
            </div>
        </section>
        <section class="l-section l-section--s-2">
            <div class="l-inner">
                {% include "partials/section-2.tpl" %}
            </div>
        </section>
        <section class="l-section l-section--s-3">
            <div class="l-inner u-gutter-sm-top u-gutter-sm-bottom">
                {% include "partials/section-3.tpl" %}
            </div>
        </section>
        <section class="l-section l-section--s-4">
            <div class="l-inner u-gutter-l-top u-gutter-m-bottom">
                {% include "partials/section-4.tpl" %}
            </div>
        </section>
        <section class="l-section l-section--s-5">
            <div class="l-inner">
                {% include "partials/section-5.tpl" %}
            </div>
        </section>
        <section class="l-section l-section--s-6">
            <div class="l-inner u-gutter-s-top u-gutter-md-bottom">
                {% include "partials/section-6.tpl" %}
            </div>
        </section>
        <section class="l-section l-section--s-7">
            <div class="l-inner u-gutter-lg-top u-gutter-lg-bottom">
                {% include "partials/section-7.tpl" %}
            </div>
        </section>
        <section class="l-section l-section--s-8">
            <div class="l-inner u-gutter-m-top u-gutter-l-bottom">
                {% include "partials/section-8.tpl" %}
            </div>
        </section>
        {% import "partials/layouts/footer.tpl" as l_footer %}
        {{ l_footer.render() }}
    </body>
</html>
